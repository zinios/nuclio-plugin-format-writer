<?hh
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\writer
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\format\driver\common\FormatException;
	use nuclio\plugin\format\driver\common\CommonInterface;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	
	<<factory>>
	class Writer extends Plugin
	{
		public static function getInstance(...$args):Writer
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		/**
		 * @depricated
		 */
		public static function write(string $filename, mixed $content):bool
		{
			return self::setContent($filename,$contents);
		}

		public static function setContent(string $filename, mixed $content, Map<mixed,mixed> $options=Map{}, ?string $forceExt=null):bool
		{
			$extension	=$forceExt ?? pathinfo($filename,PATHINFO_EXTENSION);
			$driver		=ProviderManager::request('format::'.$extension);
			if ($driver instanceof CommonInterface)
			{
				return $driver->write($filename,$content);
			}
			else
			{
				throw new FormatException(sprintf('No driver found for "%s" file type.',$extension));
			}
		}
	}
}
