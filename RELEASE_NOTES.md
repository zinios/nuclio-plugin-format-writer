Release Notes
-------------
2.0.1
-----
* Fixed undefined variable. Use of $ext instead of $extension.

2.0.0
-----
* Rewrote the reader. Code is now much simpler and only relies on the one mehtod to work. Now uses "setContent" as the writing method. "write" has been depricated. Also now uses providers rather than hard-coded driver references.

1.0.0
-----
* Initial Release.